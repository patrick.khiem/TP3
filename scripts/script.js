document.getElementById("id-jour").onchange = function() {checkJour()};
document.getElementById("id-mois").onchange = function() {checkMois()};
document.getElementById("id-an").onchange = function() {checkAn()};
document.getElementById("id-nas").onchange = function() {checkNas()};
document.getElementById("id-code-uqam").onchange = function() {checkCode()};
document.getElementById("id-code").onchange = function() {checkCode2()};
document.getElementById("id-courriel").onchange = function() {checkCourriel()};
document.getElementById("id-tel-domicile").onchange = function() {checkTel()};
document.getElementById("id-tel-cell").onchange = function() {checkTel2()};
document.getElementById("id-tel-trav").onchange = function() {checkTel3()};
document.getElementById("id-postal").onchange = function() {checkPost()};
document.getElementById("id-postal2").onchange = function() {checkPost2()};

function valideForm() {
	pass = true;
	checkJour();
	checkMois();
	checkAn();
	checkCode();
	checkCode2();
	checkCourriel();
	checkPost();
	checkOblig('id-famille', 0, 'Nom invalide');
	checkOblig('id-prenom', 1, 'Prénom invalide');
	check2Option('sexe', 0, 'Selectionnez une réponse');
	checkOblig('id-ville', 3, 'À remplir');
	checkParent('id-nom-pere', 'id-prenom-pere', 4, 5, 'À remplir');
	checkParent('id-nom-mere', 'id-prenom-mere', 6, 7, 'À remplir');
	checkCitoyen();
	checkNas();
	checkLangue('id-langue-parle', 'id-parle-autre', 8);
	checkLangue('id-langue-mat', 'id-mat-autre', 9);
	checkPhone();
	checkAdr();
	checkTrim();
	checkChoix();
	checkChoix2('prog-titre2', 'prog-code2', 'statut2', 15);
	checkChoix2('prog-titre3', 'prog-code3', 'statut3', 16);
	checkExt();
	checkDip();
	checkUni('uni-uni', 'uni-nom', 'uni-disc', 'uni-inst', 'uni-pays', 'uni-mois', 'uni-an', 'uni-cred', 19, 'uni-debut', 'uni-fin', 'freq4');
	checkUni('autre-uni', 'autre-nom', 'autre-disc', 'autre-inst', 'autre-pays', 'autre-mois', 'autre-an', 'autre-cred', 20, 'autre-debut', 'autre-fin', 'freq5');
	checkEmploi ('emp-check', 'emp-nom', 'emp-fonct', 'emp-type', 'emp-statut', 21, 'emp-debut-mois','emp-debut-an', 'emp-fin-mois','emp-fin-an', 'freq6');
	checkEmploi ('emp-check2', 'emp-nom2', 'emp-fonct2', 'emp-type2', 'emp-statut2', 22, 'emp-debut-mois2','emp-debut-an2', 'emp-fin-mois2','emp-fin-an2', 'freq7');
	checkEmploi ('emp-check3', 'emp-nom3', 'emp-fonct3', 'emp-type3', 'emp-statut3', 23, 'emp-debut-mois3','emp-debut-an3', 'emp-fin-mois3','emp-fin-an3', 'freq8');
	checkFreq('rens-debut', 'rens-fin', 'freq');
	if (!pass){
		alert("Formulaire incomplet ou invalide");
	}
	return pass;
}


function checkEmploi(check, nom, fonct, type, statut, numero, mois1, an1, mois2, an2, number){
	var a = document.getElementById(check).checked;
	var b = document.getElementById(nom).value;
	var c = document.getElementById(fonct).value;
	var d = document.getElementsByName(type)[0].checked;
	var e = document.getElementsByName(type)[1].checked;
	var f = document.getElementsByName(type)[2].checked;
	var g = document.getElementsByName(statut)[0].checked;
	var h = document.getElementsByName(statut)[1].checked; 
	var j = document.getElementById(mois1).value;
	var k = document.getElementById(an1).value;
	var l = document.getElementById(mois2).value;
	var m = document.getElementById(an2).value;
	if (a && (b.length == 0 || c.length == 0 || j.length == 0 ||  k.length == 0 ||  l.length == 0 ||  m.length == 0 || (!d && !e && !f) || (!g && !h))){
		document.getElementsByClassName("msg")[numero].innerHTML = "Remplissez les informations";
		pass = false;
	}else{
		document.getElementsByClassName("msg")[numero].innerHTML = null;
		checkFreq2(mois1, an1, mois2, an2, number);
	}
}


function checkUni(check, nom, disc, inst, pays, mois, an, cred, numero, debut, fin, freq){
	var a = document.getElementById(check).checked;
	var b = document.getElementById(nom).value;
	var c = document.getElementById(disc).value;
	var d = document.getElementById(inst).value;
	var e = document.getElementById(pays).value;
	var f = document.getElementById(mois).value;
	var g = document.getElementById(an).value;
	var h = document.getElementById(cred).value; 
	var i = document.getElementById(debut).value;
	var j = document.getElementById(fin).value;
	if (a && (h.length == 0 || b.length == 0 || c.length == 0 || d.length == 0 || e.length == 0 || f.length == 0 || g.length == 0 || i.length == 0 || j.length == 0)){
		document.getElementsByClassName("msg")[numero].innerHTML = "Remplissez les informations";
		pass = false;
	}else{
		document.getElementsByClassName("msg")[numero].innerHTML = null;
		checkFreq(debut, fin, freq);
	}

}

function checkExt(){
	var check = document.getElementById("exterieur").checked;
	var nom = document.getElementById("rens-nom").value;
	var disc = document.getElementById("rens-disc").value;
	var inst = document.getElementById("rens-inst").value;
	var pays = document.getElementById("rens-pays").value;
	var x = document.getElementById("dip-obt-debut").value;
	var y = document.getElementById("dip-obt-fin").value;
	var debut = document.getElementById("dip-debut").value;
	var fin = document.getElementById("dip-fin").value;
	if (check && (nom.length == 0 || disc.length == 0 || inst.length == 0 || pays.length == 0 || x.length == 0 || y.length == 0 || debut.length == 0 || fin.length == 0)){
		document.getElementsByClassName("msg")[17].innerHTML = "Remplissez les informations";
		pass = false;
	}else{
		document.getElementsByClassName("msg")[17].innerHTML = null;
		checkFreq("dip-debut", "dip-fin", 'freq2');
	}
}

function checkDip(){
	var check = document.getElementsByName("dip-type")[0].checked;
	var check2 = document.getElementsByName("dip-type")[1].checked;
	var nom = document.getElementById("dip-nom").value;
	var disc = document.getElementById("dip-disc").value;
	var inst = document.getElementById("dip-inst").value;
	var pays = document.getElementById("dip-pays").value;
	var x = document.getElementById("dip-mois").value;
	var y = document.getElementById("dip-an").value;
	var debut = document.getElementById("dip-debut2").value;
	var fin = document.getElementById("dip-fin2").value;
	if ((check || check2) && (nom.length == 0 || disc.length == 0 || inst.length == 0 || pays.length == 0 || x.length == 0 || y.length == 0 || debut.length == 0 || fin.length == 0)){
		document.getElementsByClassName("msg")[18].innerHTML = "Remplissez les informations";
		pass = false;
	}else{
		document.getElementsByClassName("msg")[18].innerHTML = null;
		checkFreq("dip-debut2", "dip-fin2", 'freq3');
	}
}
function checkChoix(){
	var x = document.getElementById("prog-titre1").value;
	var y = document.getElementById("prog-code1").value;
	var z = document.getElementsByName("statut1");
	
	if ( x.length == 0 || y.length == 0 || (!z[0].checked && !z[1].checked)){
		document.getElementsByClassName("msg")[14].innerHTML = "Remplissez votre premier choix";
		pass = false;
	}else{
		document.getElementsByClassName("msg")[14].innerHTML = null;
	}
	
}

function checkChoix2(titre, code, statut, numero){
	var x = document.getElementById(titre).value;
	var y = document.getElementById(code).value;
	var z = document.getElementsByName(statut);
	
	if ( x.length + y.length != 0 && ((x.length == 0 || y.length == 0) || (!z[0].checked && !z[1].checked))){
		document.getElementsByClassName("msg")[numero].innerHTML = "Remplissez votre choix";
		pass = false;
	}else if ((z[0].checked || z[1].checked) && (x.length == 0 || y.length == 0)){
		document.getElementsByClassName("msg")[numero].innerHTML = "Remplissez votre choix";
		pass = false;
	}else{
		document.getElementsByClassName("msg")[numero].innerHTML = null;
	}
	
}

function checkTrim(){
	var x = document.getElementsByName("trim")[0];
	var y = document.getElementsByName("trim")[1];
	var z = document.getElementsByName("trim")[2];
	
	if (!x.checked && !y.checked && !z.checked){
		document.getElementsByClassName("msg")[12].innerHTML = "Choisissez un trimestre";
		pass = false;
	}else if ( document.getElementById("prog-an").value.length == 0 ){
		document.getElementsByClassName("msg")[13].innerHTML = "Présicez une année";
		pass = false;
	}else{
		document.getElementsByClassName("msg")[12].innerHTML = null;
		document.getElementsByClassName("msg")[13].innerHTML = null;
	}
}
function checkAdr(){
	var x = document.getElementById("id-adresse").value;
	var y = document.getElementById("id-muni").value;
	var z = document.getElementById("id-pays").value;
	var p = document.getElementById("id-postal").value;
	
	if ( x.length == 0 || y.length == 0 || z.length == 0 || p.length == 0 ){
		document.getElementsByClassName("msg")[11].innerHTML = "Remplissez tous les champs";
		pass = false;
	}else{
		document.getElementsByClassName("msg")[11].innerHTML = null;
	}
}

function checkPhone(){
	var x = document.getElementById("id-tel-domicile").value;
	var y = document.getElementById("id-tel-cell").value;
	var z = document.getElementById("id-tel-trav").value;
	
	if ( x.length == 0 && y.length == 0 && z.length == 0 ){
		document.getElementsByClassName("msg")[10].innerHTML = "Vous devez avoir au moins un numéro";
		pass = false;
	}else{
		document.getElementsByClassName("msg")[10].innerHTML = null;
		checkTel();
		checkTel2();
		checkTel3();
	}
}
function checkLangue(id, id2, numero){
	var x = document.getElementById(id).value;
	var y = document.getElementById(id2).value;
	if (x == "Autre" && y.length == 0){
		document.getElementsByClassName("msg")[numero].innerHTML = "Précisez la langue";
		pass = false;
	}else{
		document.getElementsByClassName("msg")[numero].innerHTML = null;
	}
}

function checkParent(nom, prenom, numero, numero2, msg){
	var x = document.getElementById(nom).value;
	var y = document.getElementById(prenom).value;
	
	if( x.length == 0 && y.length == 0) {
		document.getElementsByClassName("msg")[numero].innerHTML = null;
		document.getElementsByClassName("msg")[numero2].innerHTML = null;
	}else if (x.length == 0){
		document.getElementsByClassName("msg")[numero].innerHTML = msg;
		pass = false;
	}else if (y.length == 0){
		document.getElementsByClassName("msg")[numero2].innerHTML = msg;
		pass = false;
	}else{
		document.getElementsByClassName("msg")[numero].innerHTML = null;
		document.getElementsByClassName("msg")[numero2].innerHTML = null;
	}
}
function checkCitoyen(){
	var x = document.getElementsByName("cit")[0];
	var y = document.getElementsByName("cit")[1];
	var z = document.getElementById("id-cit").value.length;
	
	
	if(!x.checked && !y.checked){
		document.getElementsByClassName("msg")[2].innerHTML = "Selectionnez une réponse";
		pass = false;
	}else if (y.checked && z == 0){
		document.getElementsByClassName("msg")[2].innerHTML = "Précisez";
		pass = false;
	}else{
		document.getElementsByClassName("msg")[2].innerHTML = null;
	}
	
}
function checkOblig(id, numero, message) {
	var x = document.getElementById(id).value.length;
	if (x==0){
		document.getElementsByClassName("msg")[numero].innerHTML = message;
		pass = false;
	}else{
		document.getElementsByClassName("msg")[numero].innerHTML = null;
	}
}

function check2Option(name, numero, message){
	var x = document.getElementsByName(name)[0];
	var y = document.getElementsByName(name)[1];	
	if (!x.checked && !y.checked){
		document.getElementsByClassName("selection")[numero].innerHTML = message;
		pass = false;
	}else{
		document.getElementsByClassName("selection")[numero].innerHTML = null;
	}
}

function checkJour() {
    var x = document.getElementById("id-jour").value;
	var y = +x;
	if(x > 31 || x <= 0 || isNaN(y)){
		document.getElementById("date").innerHTML = "Date invalide";
		pass = false;
	}else{
		document.getElementById("date").innerHTML = null;
	}	
}

function checkMois() {
    var x = document.getElementById("id-mois").value;  
	var y = +x;
	if(x > 12 || x <= 0 || isNaN(y)){
		document.getElementById("date").innerHTML = "Date invalide";
		pass = false;
	}else{
		document.getElementById("date").innerHTML = null;
	}	
}

function checkAn() {
    var x = document.getElementById("id-an").value; 
	var y = +x;
	if(x > 2005 || x <= 0 || x < 1868 || isNaN(y)){
		document.getElementById("date").innerHTML = "Date invalide";
		pass = false;
	}else{
		document.getElementById("date").innerHTML = null;
	}	
}

function checkNas() {
    var x = document.getElementById("id-nas").value;
	var y = +x;
	
	if(x.length == 0){
		document.getElementById("nas").innerHTML = null;
	}else if(isNaN(y) || x.length != 9){
		document.getElementById("nas").innerHTML = "NAS invalide";
		pass = false;
	}else{
		document.getElementById("nas").innerHTML = null;
	}	
}

function checkCode() {
    var x = document.getElementById("id-code-uqam").value;
	if (x.length == 0){
		document.getElementById("code-uqam").innerHTML = null;
	}else if(x.length != 12){
		document.getElementById("code-uqam").innerHTML = "Code invalide";
		pass = false;
	}else{
		var y = x.substr(0,3);
		var z = +x.substr(4,11);
		if(!(!/[^A-Z]/.test(y)) || isNaN(z)){
			document.getElementById("code-uqam").innerHTML = "Code invalide";
			pass = false;
		}else{
			document.getElementById("code-uqam").innerHTML = null;
		}
	}	
}
function checkCode2() {
    var x = document.getElementById("id-code").value;
	if (x.length == 0){
		document.getElementById("code").innerHTML = "À remplir";
		pass = false;
	}else if(x.length != 12){
		document.getElementById("code").innerHTML = "Code invalide";
		pass = false;
	}else{
		var y = x.substr(0,3);
		var z = +x.substr(4,11);
		if(!(!/[^A-Z]/.test(y)) || isNaN(z)){
			document.getElementById("code").innerHTML = "Code invalide";
			pass = false;
		}else{
			document.getElementById("code").innerHTML = null;
		}
	}	
}

function checkCourriel() {
    var x = document.getElementById("id-courriel").value;
	if (x.length == 0){
		document.getElementById("courriel").innerHTML = null;
	}else if(!x.includes("@")){
		document.getElementById("courriel").innerHTML = "Courriel invalide";
		pass = false;
	}else{
		document.getElementById("courriel").innerHTML = null;
	}
}

function checkTel() {
    var tel = document.getElementById("id-tel-domicile").value;
	if (tel.length == 0){
		document.getElementById("teldom").innerHTML = null;
	}else if(tel.length != 12){
		document.getElementById("teldom").innerHTML = "Numéro invalide";
		pass = false;
	}else{
		var x = +tel.substr(0,3);
		var y = +tel.substr(4,3);
		var z = +tel.substr(8,11);		
		if( isNaN(x) || isNaN(y) || isNaN(z) || tel.charAt(3) != '-' || tel.charAt(7) != '-'){
			document.getElementById("teldom").innerHTML = "Numéro invalide";
			pass = false;
		}else{
			document.getElementById("teldom").innerHTML = null;
		}
	}
}

function checkTel2() {
    var tel = document.getElementById("id-tel-cell").value;
	if (tel.length == 0){
		document.getElementById("telcell").innerHTML = null;
    }else if(tel.length != 12){
		document.getElementById("telcell").innerHTML = "Numéro invalide";
		pass = false;
	}else{
		var x = +tel.substr(0,3);
		var y = +tel.substr(4,3);
		var z = +tel.substr(8,11);		
		if( isNaN(x) || isNaN(y) || isNaN(z) || tel.charAt(3) != '-' || tel.charAt(7) != '-'){
			document.getElementById("telcell").innerHTML = "Numéro invalide";
			pass = false;
		}else{
			document.getElementById("telcell").innerHTML = null;
		}
	}
}

function checkTel3() {
    var tel = document.getElementById("id-tel-trav").value;
	if (tel.length == 0){
		document.getElementById("teltrav").innerHTML = null;
	}else if(tel.length != 12){
		document.getElementById("teltrav").innerHTML = "Numéro invalide";
		pass = false;
	}else{
		var x = +tel.substr(0,3);
		var y = +tel.substr(4,3);
		var z = +tel.substr(8,11);		
		if( isNaN(x) || isNaN(y) || isNaN(z) || tel.charAt(3) != '-' || tel.charAt(7) != '-'){
			document.getElementById("teltrav").innerHTML = "Numéro invalide";
			pass = false;
		}else{
			document.getElementById("teltrav").innerHTML = null;
		}
	}
}

function checkPost() {
    var post = document.getElementById("id-postal").value;	
	if(post.length != 7){
		document.getElementById("postal").innerHTML = "Code postale invalide";
		pass = false;
	}else{
		var x = post.charAt(0) + post.charAt(2) + post.charAt(5);
		var y = +(post.charAt(1) + post.charAt(4) + post.charAt(6));
	    var z = post.charAt(3);
		if(isNaN(y) || z != ' ' || !(!/[^A-Z]/.test(x))){
			document.getElementById("postal").innerHTML = "Code postale invalide";
			pass = false;
		}else{
			document.getElementById("postal").innerHTML = null;
		}
	}
}

function checkPost2() {
    var post = document.getElementById("id-postal2").value;	
	if(post.length != 7){
		document.getElementById("postal2").innerHTML = "Code postale invalide";
		pass = false;
	}else{
		var x = post.charAt(0) + post.charAt(2) + post.charAt(5);
		var y = +(post.charAt(1) + post.charAt(4) + post.charAt(6));
	    var z = post.charAt(3);
		if(isNaN(y) || z != ' ' || !(!/[^A-Z]/.test(x))){
			document.getElementById("postal2").innerHTML = "Code postale invalide";
			pass = false;
		}else{
			document.getElementById("postal2").innerHTML = null;
		}
	}
}

function checkFreq(a, b, c) {
    var x = +document.getElementById(a).value;
	var y = +document.getElementById(b).value;	
	if(isNaN(y) || isNaN(x) || x > y){
		document.getElementById(c).innerHTML = "Dates invalide";
		pass = false;
	}else{
		document.getElementById(c).innerHTML = null;
	}	
}

function checkFreq2(a, b, c, d, e) {
	var moisDebut = +document.getElementById(a).value;
	var anDebut = +document.getElementById(b).value;
	var moisFin = +document.getElementById(c).value;
	var anFin = +document.getElementById(d).value;
	
	if(isNaN(moisDebut) || isNaN(anDebut) || isNaN(moisFin) || isNaN(anFin) || anDebut > anFin) {
		document.getElementById(e).innerHTML = "Dates invalide";
		pass = false;
	}else if(anDebut == anFin && moisDebut > moisFin){
		document.getElementById(e).innerHTML = "Dates invalide";
		pass = false;		
	}else{
		document.getElementById(e).innerHTML = null;	
	}
}